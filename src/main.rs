mod cli;

use std::process::ExitCode;
use rustyline::error::ReadlineError;
use rustyline::DefaultEditor;
use owo_colors::OwoColorize;
use rainfuck::BrainfuckVM;
use piglog::prelude::*;
use clap::Parser;
use fspp::*;

fn main() -> ExitCode {
    let args = cli::Cli::parse();

    match args.command {
        cli::Command::File { file_path } => {
            let path = Path::new(&file_path);

            if path.exists() == false {
                piglog::fatal!("File doesn't exist!");

                return ExitCode::FAILURE;
            }

            let code = file::read(&path).unwrap();

            let mut vm = brainfuck_vm();
            vm.process(&code);
        },
        cli::Command::Eval { code } => {
            let mut vm = brainfuck_vm();
            vm.process(&code);
        },
        cli::Command::Repl => repl(),
    };

    return ExitCode::SUCCESS;
}

fn repl() {
    let mut left = String::from("[");
    left = left.bright_black().bold().to_string();
    
    let mut right = String::from("]");
    right = right.bright_black().bold().to_string();
    
    let mut title = String::from("Brainfuck");
    title = title.bright_cyan().bold().to_string();
    
    let mut prompt = String::from(">>>");
    prompt = prompt.bright_green().bold().to_string();

    let mut editor = DefaultEditor::new().unwrap();

    let mut vm = brainfuck_vm();

    loop {
        let code = match editor.readline(&format!("{left}{title}{right} {prompt} ")) {
            Ok(o) => o,
            Err(e) => {
                match e {
                    ReadlineError::Eof => return,
                    ReadlineError::Interrupted => { "".to_string() },
                    _ => "".to_string(),
                }
            },
        };

        editor.add_history_entry(&code).unwrap();

        if code == "?" || code == "help" {
            println!("{}", "Commands:".bold().underline());
            println!("    {} - Print out the VM's cells", "cells".bright_magenta().bold());
            println!("    {} - Run a command (without arguments) (example: :/clear)", ":/".bright_magenta().bold());
            println!("");
            println!("Hit '{}' to exit REPL.", "CTRL + D".bright_blue().bold());

            continue;
        }

        if code == "cells" {
            let mut cells_vec = vm.cells.to_vec();

            if let Some(s) = cells_vec.iter().rposition(|x| *x != 0) {
                cells_vec.truncate(s + 1);
            }

            print!("{}", "[".bright_black().bold());

            for i in 0..cells_vec.len() {
                print!("{}", cells_vec[i].bright_cyan().bold());

                if i < cells_vec.len() - 1 {
                    print!("{} ", ",".bright_black().bold());
                }
            }

            println!("{}", "]".bright_black().bold());

            continue;
        }

        if code.starts_with(":/") {
            let _ = std::process::Command::new(code.get(2..).unwrap_or("")).status();

            continue;
        }

        vm.process(&code);
    }
}

fn brainfuck_vm() -> BrainfuckVM {
    let args = cli::Cli::parse();

    let mut vm = BrainfuckVM::new();
    vm.debug = args.debug;
    vm.prompt_display = args.prompt_display;

    return vm;
}
