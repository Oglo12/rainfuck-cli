use clap::{ Parser, Subcommand };

#[derive(Parser)]
#[command(author, version, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Command,

    #[clap(short, long, global = true)]
    /// Enable debug messages
    pub debug: bool,

    #[clap(short, long, global = true, default_value = "")]
    /// User input prompt prefix (for the user input command ',')
    pub prompt_display: String,
}

#[derive(Subcommand)]
pub enum Command {
    /// Run a file (Brainfuck files end in .bf)
    File { file_path: String },
    /// Evaluate Brainfuck code
    Eval { code: String },
    /// Create a Brainfuck REPL
    Repl,
}
