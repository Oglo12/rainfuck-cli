# Rainfuck CLI

Run Brainfuck in the terminal!



# Usage:

Run a file: `rainfuck-cli file <FILE_PATH>`

Evaluate Brainfuck code: `rainfuck-cli eval <CODE>`

Enter a Brainfuck REPL: `rainfuck-cli repl`

If you enter a REPL, type `help` or `?` to learn how to use it for more than evaluation.
